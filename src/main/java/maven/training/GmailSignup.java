package maven.training;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Mouse;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Sleeper;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.steadystate.css.parser.Locatable;

public class GmailSignup {
	WebDriverSetup driverSetup = new WebDriverSetup();

	public static void main(String[] args) {

	}

	
	@Test
	public void ts01() throws InterruptedException {

		String url = "http://www.google.com";
		WebDriver driver = driverSetup.openBrowser(url); // Open "www.google.com"
		String titlePage = driver.getTitle();
		System.out.println(titlePage);
		assertEquals("Google", titlePage);
		
		driver.findElement(By.cssSelector("a.gb_P")).click(); // Click "Sign In"
		assertEquals("Gmail", driver.getTitle());
		
		driver.findElement(By.cssSelector("div.IMH1vc.lUHSR.Hj2jlf")).click(); // Click "More options"
		
		//click create account

		WebElement elementToWait = driver.findElement(By.xpath("//*[@id='SIGNUP']"));
		WebDriverWait myWaitVar = new WebDriverWait(driver, 15);
		myWaitVar.until(ExpectedConditions.visibilityOf(elementToWait));
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id='SIGNUP']")).click();

		//get the webelements
		Thread.sleep(3000);
		WebElement elemFirstName = driver.findElement(By.id("FirstName"));
		WebElement elemLastName = driver.findElement(By.id("LastName"));
		WebElement elemGmailAddress = driver.findElement(By.id("GmailAddress"));
		WebElement elemPasswd = driver.findElement(By.id("Passwd"));
		WebElement elemPasswdAgain = driver.findElement(By.id("PasswdAgain"));
		WebElement elemBirthMonth = driver.findElement(By.cssSelector("div[title='Birthday']"));
		WebElement elemBirthMonthCaption = driver.findElement(By.cssSelector("div[title='Birthday'] div.goog-flat-menu-button-caption"));
		WebElement elemBirthDay = driver.findElement(By.id("BirthDay"));
		WebElement elemBirthYear = driver.findElement(By.id("BirthYear"));
		WebElement elemGender = driver.findElement(By.cssSelector("div[title='Gender']"));
		WebElement elemGenderCaption = driver.findElement(By.cssSelector("div[title='Gender'] div.goog-flat-menu-button-caption"));
		WebElement elemFlag = driver.findElement(By.cssSelector("div.i18n-phone-flag"));
		WebElement elemNumber = driver.findElement(By.id("RecoveryPhoneNumber"));
		WebElement elemLocation = driver.findElement(By.cssSelector("div[title='Location']"));
		WebElement elemLocationCaption = driver.findElement(By.cssSelector("div[title='Location'] div.goog-flat-menu-button-caption"));
		WebElement elemSubmitButton = driver.findElement(By.id("submitbutton"));
		WebElement elemPrivacy = driver.findElement(By.id("tos-div"));
		
		elemFirstName.sendKeys("Alon");
		assertNotNull(elemFirstName.getAttribute("value")); //check if not null 
		Thread.sleep(1000);
		
		elemLastName.sendKeys("Buenaventura");
		assertNotNull(elemLastName.getAttribute("value")); //check if not null
		Thread.sleep(1000);
		
		elemGmailAddress.sendKeys("isla.buenaventura");
		assertNotNull(elemGmailAddress.getAttribute("value")); //check if not null
		elemPasswd.click();
		Thread.sleep(1000);
		
		WebElement elemGmailError = driver.findElement(By.id("errormsg_0_GmailAddress"));
		myWaitVar.until(ExpectedConditions.visibilityOf(elemGmailError));
		String errorGmail = elemGmailError.getText();
		assertEquals("Someone already has that username. Note that we ignore periods and capitalization in usernames. Try another?", errorGmail); //check error message after click
		Thread.sleep(1000);
		
		elemGmailAddress.clear();
		elemGmailAddress.sendKeys("alon.buenaventura");
		assertEquals("alon.buenaventura", elemGmailAddress.getAttribute("value")); //check if replaced
		Thread.sleep(1000);
		
		elemPasswd.click();
		WebElement elemGmailError2 = driver.findElement(By.id("errormsg_0_GmailAddress"));
		assertEquals("", elemGmailError2.getText()); //assert if no error message
		Thread.sleep(1000);
		
		elemPasswd.sendKeys("123456");
		assertNotNull(elemPasswd.getAttribute("value")); //assert if not null
		Thread.sleep(1000);
		
		elemPasswdAgain.click();
		WebElement errorShortPasswd = driver.findElement(By.id("errormsg_0_Passwd"));
		myWaitVar.until(ExpectedConditions.visibilityOf(errorShortPasswd));
		String errorPasswd = errorShortPasswd.getText();
		assertEquals("Short passwords are easy to guess. Try one with at least 8 characters.", errorPasswd);//check error message
		Thread.sleep(1000);
		
		elemPasswd.clear();
		elemPasswd.sendKeys("OogaOogaOogaShaka");
		assertEquals("OogaOogaOogaShaka", elemPasswd.getAttribute("value")); //check if replaced
		Thread.sleep(1000);
		
		elemPasswdAgain.sendKeys("Shaka");
		assertNotNull(elemPasswdAgain.getAttribute("value")); //assert if not null
		Thread.sleep(1000);
		
		elemGmailAddress.click(); //navigate to another element
		WebElement errorPasswdAgain = driver.findElement(By.id("errormsg_0_PasswdAgain"));
		myWaitVar.until(ExpectedConditions.visibilityOf(errorPasswdAgain));
		String errorPwAgain = errorPasswdAgain.getText();
		assertEquals("These passwords don't match. Try again?", errorPwAgain); //check error message
		Thread.sleep(1000);
		
		elemPasswdAgain.clear();
		elemPasswdAgain.sendKeys("OogaOogaOogaShaka");
		assertEquals("OogaOogaOogaShaka", elemPasswdAgain.getAttribute("value")); //assert if replaced
		Thread.sleep(1000);
		
		elemBirthMonth.click();
		WebElement elemBirthJanuary = driver.findElement(By.cssSelector("div.goog-menu-vertical div[id=':1'"));
		myWaitVar.until(ExpectedConditions.visibilityOf(elemBirthJanuary));
		elemBirthJanuary.click();
		assertEquals("January", elemBirthMonthCaption.getText());
		Thread.sleep(1000);
		
		elemBirthDay.sendKeys("1");
		assertNotNull(elemBirthDay.getAttribute("value")); //assert if not null
		Thread.sleep(1000);
		
		elemBirthYear.sendKeys("1997");
		assertNotNull(elemBirthYear.getAttribute("value")); //assert if not null
		Thread.sleep(1000);
		
		elemGender.click();
		WebElement elemGenderMale = driver.findElement(By.cssSelector("div[id=':f'"));
		myWaitVar.until(ExpectedConditions.visibilityOf(elemGenderMale));
		elemGenderMale.click();
		assertEquals("Male", elemGenderCaption.getText());
		Thread.sleep(1000);
		
		elemNumber.sendKeys("9175550044");
		assertNotNull(elemNumber.getAttribute("value")); //assert if not null
		Thread.sleep(1000);
		
		elemLocation.click();
		WebElement elemPhilippines = driver.findElement(By.cssSelector("div.goog-menu-vertical div[id=':5k'"));
		myWaitVar.until(ExpectedConditions.visibilityOf(elemPhilippines));
		elemPhilippines.click();
		assertEquals("Philippines", elemLocationCaption.getText()); //check location = Philippines
		Thread.sleep(3000);
		elemSubmitButton.sendKeys(Keys.ENTER);
		/*
		do {
			elemSubmitButton.click();
			Thread.sleep(3000);
		} while ("display: none;".equals(elemPrivacy.getAttribute("style")));
				*/
	}

	/*
	 * Click "Create account" Enter "Alon" in the First Name field Enter
	 * "Buenaventura" in the Last Name field Enter "isla.buenaventura" in the
	 * "Choose your username" field Click "Create a password" field Replace
	 * "Choose your username" entry with "alon.buenaventura" Click
	 * "Create a password" field Enter "123456" in the "Create a password" field
	 * Click "Confirm your password" field Replace "Create a password" entry
	 * with "OogaOogaOogaShaka" Enter "Shaka" in "Confirm you password" field
	 * Click "Confirm you password" field Replace "Confirm you password" entry
	 * with "OogaOogaOogaShaka" Select "January" in the "Birthday - Month"
	 * dropdown Enter "1" in "Birthday - Day" field Enter "1997" in
	 * "Birthday - Year" field Select "Male" in "Gender" dropdown Select
	 * "Philippines" in the "Flag" dropdown of "Mobile phone" field Enter
	 * "9175550044" Select "Philippines" in the "Location" field Click
	 * "Next step" button
	 */

}

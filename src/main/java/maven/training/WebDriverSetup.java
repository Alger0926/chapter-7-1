package maven.training;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDriverSetup {
	public WebDriver openBrowser(String url) {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\ALManalo\\Documents\\maven.training\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get(url);
		driver.manage().window().maximize();
		return driver;
	}
}
